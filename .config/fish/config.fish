set PATH $PATH /home/random/.local/bin

# pyenv
set PYENV_ROOT $HOME/.pyenv
set -x PATH $PYENV_ROOT/shims $PYENV_ROOT/bin $PATH
# pyenv init - | source
# status --is-interactive; and source (pyenv virtualenv-init -|psub)
pyenv rehash

# abbriviations
# matbe add check for OS?
# dnf
abbr -a dnfu sudo dnf update -y
abbr -a dnfi sudo dnf install -y
abbr -a dnfr sudo dnf remove -y
# git
abbr -a gcl git clone --recurse-submodules
abbr -a gaa git add --all
abbr -a gaa git add --all


# aliases
# config managment
set -Ux DOTFILE_FOLDER $HOME/.dotfiles
if [ -d "$DOTFILE_FOLDER" ]
	git --git-dir=$DOTFILE_FOLDER --work-tree=$HOME config --local status.showUntrackedFiles no
end

balias cfgi 	"git init --bare $DOTFILE_FOLDER"
balias cfg 	"git --git-dir=$DOTFILE_FOLDER --work-tree=$HOME"
balias cfgp 	"git --git-dir=$DOTFILE_FOLDER --work-tree=$HOME push --set-upstream origin master"

function cfgc --description "clone dotfiles" --argument git_link
	if [ -z "$git_link" ]
		echo "`git_link` can't be empty"
		return 1
	else
		git clone --bare $git_link $DOTFILE_FOLDER
	end
end

