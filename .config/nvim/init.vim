" auto install vim-plug
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
	silent !curl
		\ -fLo
		\ ~/.local/share/nvim/site/autoload/plug.vim
		\ --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" no to vi
if &compatible
	set nocompatible
endif

set number " show line numbers

call plug#begin('~/.local/share/nvim/pluggins')

Plug 'junegunn/vim-plug' 		" vim-plug for docs only

Plug 'doums/darcula' 			" dracula theme

Plug 'luochen1990/rainbow' 		" rainbow braces

Plug 'itchyny/lightline.vim' 		" status line

Plug 'scrooloose/nerdtree' 		" file explorer
	Plug 'ryanoasis/vim-devicons' 	" addes icons

Plug 'sbdchd/neoformat' 		" formater

" Plug 'neoclide/coc.nvim', {'branch': 'release'} " completion

call plug#end()

" enable rainbow plugin
let g:rainbow_active = 1
let g:rainbow_conf = {
\	'separately': {
\		'nerdtree': 0,
\	}
\}

" nerd tree

" theme setup
if (has("termguicolors"))
	set termguicolors
endif
syntax enable
colorscheme darcula
let g:lightline = { 'colorscheme': 'darculaOriginal' } " lightline theme

